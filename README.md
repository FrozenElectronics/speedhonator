# SpeedHonator
(pronounced like speedometer)

## Licenses
All code in this repository is freely available under the license terms of the
Mozilla Public License, version 2.0 ("MPL"). All schematics, PCB files, etc.
are available freely under the Creative Commons Attribution-Sharealike 4.0
License (CC-BY-SA 4.0). You are free to modify, change, adapt, reuse, butcher,
print, burn, translate, or use as part of a ritual sacrifice any and all
files in this repository. Even for commercial profit! You can relicense any
and all portions of this repository under compatible open-source licenses,
where applicable and where possible. Attribution for any portions used
elsewhere should be to "A.M. Rowsell, Frozen Electronics".


## Problem
Speedrunning has become more and more popular. Advanced speedrunners can end up
with large streaming & YouTube audiences, as well as brand deals, donations,
etc. However, there are some speedrunners who have been caught cheating. In
almost all cases, cheating is done by splicing together video of different runs,
combining all the best sections of many runs to create a good run. Early splices
were fairly easy to detect, as most cheaters didn't know to hide things like
audio cuts or align fade-outs and fade-ins to be frame perfect.

But now that knowledge is public. YouTube channels like Apollo Legend have shown
exactly how speedrunners were caught. This is good, as it allows others to
examine existing runs and find fakes. But another tool is needed to verify
that speedrunners actually did the runs in the videos they submitted.

## Possible Solutions
In some circles, the video/stream of your speedrun must be accompanied by
video of your hands holding the controller. This allows verification of the
button presses vs the video, to see if the inputs align. This does defend
against some cheaters, but again, video splicing can defeat this.

Another way to prevent cheating is to do speedruns live in front of an
audience. While this often happens at charity events like GDQ, it's rare for
world records to be set at these events. However, they are useful in assessing
the true skill level of a player, which allows audits of their recorded
speedruns to see if they have the skills to accomplish the records they have
set.

## My Solution
This project is a hardware and software solution to the issue. A device is
placed between the controller and the console, which records all button presses
that are made per frame. It then encrypts the resulting data with a symmetric
encryption algorithm, in this case AES, and then either uploads the data to
a cloud server, or allows the files to be offloaded using USB.

These files are then uploaded along with the video. The private keys are stored
on the server, and also in a secure cryptographic co-processor in the device.

## Possible Attacks and Mitigations
I am definitely not a security or cryptology expert. I design embedded devices
like this project, but this is my first attempt at doing crypto on an
embedded design. I'm aware that, while it's unlikely that someone would try
to subvert the device, it's definitely possible, so I've tried to take a few
steps in the design to make it as hard as possible.

A) The key is stored inside a Microchip ATAES132A secure EEPROM/crypto co-
processor. The keys cannot be read out once programmed. The only viable attack
would be to decap the chip and try to read the bits out -- however, Microchip
has physical defences against this inside the chip. Also, the production
devices will be potted with hard, non re-enterable potting compound, making
accessing the chips or on-board buses difficult.

B) Encryption is done live, every 16 frames, instead of at the end of a run.
This makes intercepting the communication between the two chips more difficult
while playing live. Interfering with the data coming from the ATAES132A would
result in cryptographic and MAC errors. Interfering with the data from the
PIC32 to the ATAES132A is the only viable attack method. Possibly using 
something like a Tool-Assisted Speedrun (TAS) hardware device to input
SNES commands could defeat this method. 

C) Tamper-evident markers are used in multiple places. If the device is opened,
a bit in memory is set which will tell the cloud server that the device has
been tampered with, and to invalidate that user's key and show a warning
next to all their prior submissions (or optionally remove them entirely).
An internal battery inside the potting will also allow monitoring of the
tamper markers, even when disconnected. I've considered having thin enamel wires
throughout the potting in random orientations. If these wires are broken (as
digging through the potting is likely to do) the keys would be immediately
overwritten and all on-board data destroyed, as well as having the tamper
bit activated. That's if you manage to bypass the case-opened tamper marker.

D) ChipWhisperer type attacks have apparently been mitigated to some degree
in the A revision of the ATAES132. Multiple LC filters will be applied to the
voltage inputs to prevent current spikes from being read externally.

## Feedback
As I said, I've never done anything like this before. I know there are still
attack methods that could be used to defeat this. I'm not sure if it makes
this method completely non-viable, or if the attacks are difficult enough
in the target market to render them out of reach. The only viable attack, as
far as I can tell, would be to use something like [TASbot](https://tas.bot/),
and instead of using the extreme, frame-perfect inputs that only a computer
can do, the attacker would simply record their best attempts, and then splice
those recorded inputs together. However, this can be mitigated with the
combination of the SpeedHonator & hand cam. 

The purpose of this device is not to prevent cheating altogether -- that's
never going to happen. It's just a tool to try and help verify honest
speedrunners.
