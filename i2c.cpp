/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <xc.h>
#include "i2c.h"

i2c::i2c(uint32_t speed, uint32_t pclk) {
    double baud = (((1.0 / (2.0 * (double) speed)) - 104E-9) * (double) pclk) - 2.0;
    int i_baud = (int) (baud + 0.5); // cast/round/truncate/whatever
    I2C1BRG = i_baud; // set baud rate
    I2C1CONbits.ON = 1; // enable I2C module
    //I2C1CONbits.I2CEN = 1;

    // send a START/STOP as a test
//    I2C1CONbits.SEN = 1;
//    while (I2C1CONbits.SEN);
//    I2C1CONbits.PEN = 1;
//    while (I2C1CONbits.PEN);
}

uint8_t i2c::i2ctransact(uint8_t addr, uint16_t memAddr, uint8_t *send, \
        uint8_t *recv, uint8_t size, uint8_t mode) {
    /* Note that the address needs to be sent in 7-bit mode
     * Note also that null pointers can be used when a mode
     * doesn't require use of one of the pointers
     * Mode options:
     * 0: send single byte
     * 1: send multiple bytes
     * 2: receive single byte
     * 3: receive multiple bytes
     * 4: test mode
     *
     * Status byte codes:
     * 1: NACK on address
     * 2: NACK on data
     */
    uint8_t fullAddress, status = 0;
    uint8_t loopVar;
    const int max = 255;
    const int prime = 317;
    switch (mode) {
        case 0: // Single send
            fullAddress = addr << 1; // we are writing, thus R/W = 0
            while (I2C1STATbits.TRSTAT); // wait for any transmission to finish
            I2C1CONbits.SEN = 1; // send a start
            while (I2C1CONbits.SEN); // wait for start to complete
            I2C1TRN = fullAddress; // send address
            while (I2C1STATbits.TRSTAT); // wait for transmission
            if (I2C1STATbits.ACKSTAT == 0) {
                // acknowledge received
            } else {
                status |= 1; // NACK on address
            }
            // Send memory address
            I2C1TRN = (uint8_t)(memAddr >> 8);
            while (I2C1STATbits.TRSTAT); // wait for transmission
            I2C1TRN = (uint8_t)(memAddr & 0x00FF);
            while (I2C1STATbits.TRSTAT); // wait for transmission
            // Now send the actual data
            I2C1TRN = *send; // load buffer with data
            while (I2C1STATbits.TRSTAT); // wait for transmission
            if (I2C1STATbits.ACKSTAT == 0) {
                // acknowledge received
            } else {
                status |= 2; // NACK on data
            }
            I2C1CONbits.PEN = 1; // send STOP
            while (I2C1CONbits.PEN); // wait for stop to complete
            break;
        case 1: // Multiple send
            fullAddress = addr << 1; // we are writing, thus R/W = 0
            while (I2C1STATbits.TRSTAT); // wait for any transmission to finish
            I2C1CONbits.SEN = 1; // send a start
            while (I2C1CONbits.SEN); // wait for start to complete
            I2C1TRN = fullAddress; // send address
            while (I2C1STATbits.TRSTAT); // wait for transmission
            if (I2C1STATbits.ACKSTAT == 0) {
                // acknowledge received
            } else {
                status |= 1; // NACK on address
            }
            // Send memory address
            I2C1TRN = (uint8_t)(memAddr >> 8);
            while (I2C1STATbits.TRSTAT); // wait for transmission
            I2C1TRN = (uint8_t)(memAddr & 0x00FF);
            while (I2C1STATbits.TRSTAT); // wait for transmission
            // Now send the actual data
            for (loopVar = size; loopVar > 0; loopVar--) { // do until size hits zero
                I2C1TRN = *send; // load buffer with (next) data
                while (I2C1STATbits.TRSTAT); // wait for transmission
                if (I2C1STATbits.ACKSTAT == 0) {
                    // acknowledge received
                } else {
                    status |= 2; // NACK on data
                }
                send++; // good old pointer increment... hope it works!
            }
            I2C1CONbits.PEN = 1; // send STOP
            while (I2C1CONbits.PEN); // wait for stop to complete

            break;
        case 2: // Single receive
            fullAddress = addr << 1; // we are writing, thus R/W = 0
            while (I2C1STATbits.TRSTAT); // wait for any transmission to finish
            I2C1CONbits.SEN = 1; // send a start
            while (I2C1CONbits.SEN); // wait for start to complete
            I2C1TRN = fullAddress; // send address
            while (I2C1STATbits.TRSTAT); // wait for transmission
            if (I2C1STATbits.ACKSTAT == 0) {
                // acknowledge received
            } else {
                status |= 1; // NACK on address
            }
            // Send memory address
            I2C1TRN = (uint8_t)(memAddr >> 8);
            while (I2C1STATbits.TRSTAT); // wait for transmission
            I2C1TRN = (uint8_t)(memAddr & 0x00FF);
            while (I2C1STATbits.TRSTAT); // wait for transmission
            // Now send the actual data
            if(send != NULL) {
                I2C1TRN = *send; // load buffer with data
                while (I2C1STATbits.TRSTAT); // wait for transmission
                if (I2C1STATbits.ACKSTAT == 0) {
                    // acknowledge received
                } else {
                    status |= 2; // NACK on data
                }
            }
            // RECEIVE SECTION
            I2C1CONbits.RSEN = 1; // send restart
            while (I2C1CONbits.RSEN); // wait for restart
            fullAddress = (addr << 1) | 0x01; // address is now for reading
            I2C1TRN = fullAddress; // send address
            while (I2C1STATbits.TRSTAT); // wait for transmission
            if (I2C1STATbits.ACKSTAT == 0) {
                // acknowledge received
            } else {
                status |= 1; // NACK on address
            }
            I2C1CONbits.RCEN = 1; // enable receiever
            while (!I2C1STATbits.RBF); // wait for 8 bits to be receieved
            *recv = I2C1RCV;
            I2C1CONbits.ACKDT = 1; // change to NACK
            I2C1CONbits.ACKEN = 1; // send ACKDT
            while (I2C1CONbits.ACKEN); // wait for NACK to send

            I2C1CONbits.PEN = 1; // send STOP
            while (I2C1CONbits.PEN); // wait for stop to complete
            break;
        case 3: // Multiple receive
            fullAddress = (addr << 1); // we are writing, thus R/W = 0
            while (I2C1STATbits.TRSTAT); // wait for any transmission to finish
            I2C1CONbits.SEN = 1; // send a start
            while (I2C1CONbits.SEN); // wait for start to complete
            I2C1TRN = fullAddress; // send address
            while (I2C1STATbits.TRSTAT); // wait for transmission
            if (I2C1STATbits.ACKSTAT == 0) {
                // acknowledge received
            } else {
                status |= 1; // NACK on address
            }
            // Send memory address
            I2C1TRN = (uint8_t)(memAddr >> 8);
            while (I2C1STATbits.TRSTAT); // wait for transmission
            I2C1TRN = (uint8_t)(memAddr & 0x00FF);
            while (I2C1STATbits.TRSTAT); // wait for transmission
            // Now send the actual data
            I2C1TRN = *send; // load buffer with data
            while (I2C1STATbits.TRSTAT); // wait for transmission
            if (I2C1STATbits.ACKSTAT == 0) {
                // acknowledge received
            } else {
                status |= 2; // NACK on data
            }
            // RECEIVE SECTION
            I2C1CONbits.RSEN = 1; // send restart
            while (I2C1CONbits.RSEN); // wait for restart
            fullAddress = (addr << 1) | 0x01; // address is now for reading
            I2C1TRN = fullAddress; // send address
            while (I2C1STATbits.TRSTAT); // wait for transmission
            if (I2C1STATbits.ACKSTAT == 0) {
                // acknowledge received
            } else {
                status |= 1; // NACK on address
            }
            for (loopVar = size; loopVar > 0; loopVar--) {
                I2C1CONbits.RCEN = 1; // enable receiever
                while (!I2C1STATbits.RBF); // wait for 8 bits to be receieved
                *recv = I2C1RCV;
                recv++;
                if (loopVar > 1) {
                    I2C1CONbits.ACKDT = 0; // ACK
                    I2C1CONbits.ACKEN = 1;
                    while (I2C1CONbits.ACKEN); // wait for ACK
                } else {
                    I2C1CONbits.ACKDT = 1; // change to NACK
                    I2C1CONbits.ACKEN = 1; // send ACKDT
                    while (I2C1CONbits.ACKEN); // wait for NACK to send
                }
            }
            I2C1CONbits.PEN = 1; // send STOP
            while (I2C1CONbits.PEN); // wait for stop to complete
            break;
        case 4: // infinite test mode
            do {
                for (int i = 0; i < max; ++i) {
                    uint8_t c = (int) (i * prime) % max;
                    I2C1CONbits.SEN = 1;
                    while (I2C1CONbits.SEN);
                    I2C1TRN = c;
                    while (I2C1STATbits.TBF); // wait for transmission
                    I2C1CONbits.PEN = 1;
                    while (I2C1CONbits.PEN);
                }
            } while (size--);
            break;
        default:
            status |= 0x04;
            break;
    }
    return status;
}