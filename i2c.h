/* 
 * File:   myI2C.h
 * Author: Alexander Rowsell
 *
 * Created on May 3, 2015, 1:06 AM
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef MYI2C_H
#define MYI2C_H

#include <xc.h>

#define SINGLE_SEND     0
#define MULTIPLE_SEND	1
#define SINGLE_RECV     2
#define MULTIPLE_RECV	3
#define TEST_MODE       4
//#define SSD_MODE	5

class i2c {
public:
    i2c(uint32_t speed, uint32_t pclk);

    uint8_t i2ctransact(uint8_t addr, uint16_t memAddr, uint8_t *send, \
                        uint8_t *recv, uint8_t size, uint8_t mode);
};


#endif
