/* 
 * File:   fifo.h
 * Author: amr
 *
 * Created on July 4, 2020, 2:07 AM
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef FIFO_H
#define	FIFO_H

#include <stdint.h>
#include <stdbool.h>

template <class T, int N>
class fifo {
public:
  fifo(void);
  bool writeToBuffer(T item);
  T readFromBuffer(void);
  
private:
  T buffer[N];
  int bufferSize;
  uint8_t readIndex;
  uint8_t writeIndex;
  uint8_t usedSize;
};

template <class T, int N>
fifo<T,N>::fifo(void) {
  readIndex = 0;
  writeIndex = 0;
  usedSize = 0;
  bufferSize = N;
}

template <class T, int N>
T fifo<T,N>::readFromBuffer(void) {
  T retValue = 0;
  if(usedSize == 0) {
    // buffer is empty
    return false;
  }
  retValue = buffer[readIndex];
  readIndex++;
  if(readIndex == bufferSize) {
    readIndex = 0;
  }
  usedSize--;
  return retValue;
}

template <class T, int N>
bool fifo<T,N>::writeToBuffer(T item) {
  if(usedSize >= bufferSize) {
    // buffer is full!
    return false;
  }
  buffer[writeIndex] = item;
  writeIndex++;
  if(writeIndex == bufferSize) {
    writeIndex = 0;
  }
  usedSize++;
  return true;
}


#endif	/* FIFO_H */

