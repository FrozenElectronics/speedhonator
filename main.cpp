/* 
 * File:   main.cpp
 * Author: amr
 *
 * Created on July 4, 2020, 2:06 AM
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <cstdlib>
#include <cstdint>
#include "fuses.h"
#include <xc.h>
#include <sys/kmem.h>
#include "controller.h"
#include "fifo.h"
#include "i2c.h"
#include "ataes.h"

using namespace std;

/*
 * Pin mapping:
 * 
 * UART: (for debug)
 * U1TX = RC0/pin 25
 * 
 * I2C1: (to ATECC608A)
 * SCL = RB8/pin 44
 * SDA = RB9/pin 1
 * 
 * master SPI: (to flash)
 * SCK1 = RB14/pin 14
 * SDO1 = RB5/pin 41
 * SDI1 = RC8/pin 4
 * 
 * slave SPI: (from SNES)
 * SCK2 = RB15/pin 15
 * SDI2 = RB2/pin 23
 * /SS2 = RC4/pin 37
 * 
 * programming:
 * PGED3 = RA0/pin 19
 * PGEC3 = RA1/pin 20
 * 
 * USB:
 * RB10: D+
 * RB11: D-
 * 
 * RGB LED:
 * R = RA7/pin 13
 * G = RA8/pin 32
 * B = RA9/pin 35
 * 
 */

uint16_t dmaBuffer[16]; // twice as big, just in case

void sendChar(uint8_t c) {
    U1TXREG = c;
    while(!U1STAbits.TRMT); // wait for transmission
    return;
}

uint8_t appInfo(uint8_t *msg, uint16_t len) {
    uint16_t offset = 0;
    do {
        sendChar(*(msg+offset));
        offset++;
    } while(--len);
    return 0;
}

uint8_t initSystem(void) {
    /* set up GPIO */
    SYSKEY = 0x0;
    SYSKEY = 0xAA996655;
    SYSKEY = 0x556699AA;
    CFGCON &= ~(1<<13); // unlock PPS
    
    SDI1R = 0b0110; // RC8
    SDI2R = 0b0100; // RB2
    SS2R = 0b0111; // RC4
    RPB5R = 0b0011; // SD01
    RPC0R = 0b0001; // U1TX
    
    SYSKEY = 0x12345678; // lock SYSKEY

    /* Set up SPI1 */
    SPI1BRG = 1; // 1.5MHz
    SPI1CONbits.ENHBUF = 1; // enable enhanced buffer
    SPI1CONbits.MSTEN = 1; // master mode
    SPI1CONbits.STXISEL = 0b10; // interrupt on half-empty or more
    
    /* Set up SPI2 */
    
    SPI2CONbits.ENHBUF = 1;
    SPI2CONbits.MODE16 = 1; // 16-bit mode
    SPI2CONbits.DISSDO = 1; // receive-only
    SPI2CONbits.CKP = 1; // clock idle high
    SPI2CONbits.CKE = 1; // transition on active to idle
    SPI2CONbits.MSTEN = 0; // slave mode
    SPI2CONbits.SSEN = 1; // use /SS2
    SPI2CONbits.SRXISEL = 0b11; // interrupt on full buffer, DMA trigger
    
    /* set up UART */
    U1BRG = 38; // 9600 baud
    U1STAbits.UTXEN = 1; // enable transmitter
    U1MODEbits.ON = 1; // enable UART
    
    /* set up DMA */
    // clear all 4 DMA channel flags & IE
//    IEC1CLR = 0xF0000000;
//    IFS1CLR = 0xF0000000;
//    
//    DMACONbits.ON = 1; //enable DMA controller
//    
//    // Channel 0: SPI2 receive triggers DMA to buffer
//    DCH0ECONbits.CHSIRQ = 51; // SPI2 receive complete
//    DCH0ECONbits.SIRQEN = 1; // enable trigger
//    DCH0CONbits.CHCHN = 1; // enable chaining
//    DCH0CONbits.CHAEN = 1; // auto enable
//    DCH0SSA = KVA_TO_PA(&SPI2BUF); // source is SPI2BUF
//    DCH0DSA = KVA_TO_PA(dmaBuffer);
//    DCH0SSIZ = 1;
//    DCH0DSIZ = 16;
//    DCH0CSIZ = 16; // copy 1 sets of 2 bytes from source to destination
//    
//    IEC1SET = 0x10000000; // channel 0 interrupt enable
//    DCH0CONbits.CHEN = 1; // enable channel
    return 0;
}
int main(void) {
    if(initSystem()) {
        return 0;
    }
    i2c myi2c(100E3, 6E6), *pmyi2c = &myi2c; // enable with 100KHz clock speed
    controller SNES;
    ataes ataes132a(0x50, pmyi2c);
    SNES.updateButtonState(0x2431);
    appInfo((uint8_t *)"Testing 1 2 3\n", 14);
    
    return 0;
}

