/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "ataes.h"

ataes::ataes(uint8_t devAddr, i2c *assignedI2C) {
    this->deviceAddress = devAddr;
    this->ataes132a = assignedI2C;
    // test CRC generator
    uint8_t *crc = (uint8_t *)malloc(2*sizeof(uint8_t));
    uint8_t *packet = (uint8_t *)malloc(10*sizeof(uint8_t));
    *packet = 0x09;
    *(packet + 1) = 0x02;
    *(packet + 2) = 0x02;
    *(packet + 3) = 0x00;
    *(packet + 4) = 0x00;
    *(packet + 5) = 0x00;
    *(packet + 6) = 0x00;
    calculateCRC(7, packet, crc);
    if(*crc == 0xF9 && *(crc + 1) == 0x60) ;
    command(0, NULL, ATAES_RESET);
    command(0, NULL, ATAES_GET_STATUS);
    return;
}

uint8_t ataes::encryptData(uint8_t length, uint8_t *data, uint8_t *eData) {
    return 0;
}

uint8_t ataes::decryptData(uint8_t length, uint8_t *data, uint8_t *dData) {
    return 0;
}

uint8_t ataes::storeData(uint32_t length, uint8_t* data, uint8_t addr) {
    
}

uint8_t ataes::command(uint8_t length, uint8_t *data, uint8_t command) {
    uint8_t count = 0;
    uint8_t response = 0, waiting = 0;
    uint8_t recv[5] = {0,0,0,0,0};
    uint8_t *precv = recv;
    uint8_t *crc = (uint8_t *)malloc(2*sizeof(uint8_t));
    uint8_t *packet = (uint8_t *)malloc(length+10*sizeof(uint8_t));
    
    ataes132a->i2ctransact(deviceAddress, IOADDR_RESET_ADDR, \
                           (uint8_t *)&resetByte, NULL, 1, SINGLE_SEND); // reset command buffer address
    switch(command) {
        case ATAES_RESET:
            count = 4; // just the length, command and CRC
            *packet = count;
            *(packet + 1) = ATAES_RESET;
            calculateCRC(2, packet, crc); // 0x98 0x03
#ifdef DEBUG
	    printf("CRC: %x %x\n", *crc, *(crc+1)); // see what it actually calculated
#endif
	    memcpy((packet + 2), crc, 2); // copy crc to end of packet
            ataes132a->i2ctransact(deviceAddress, COMMAND_BUFFER_ADDR, packet, NULL, count, MULTIPLE_SEND);
            while(waiting == 0) {
                uint32_t x = 0x00001FFF;
                *precv = 0x00;
               
                if(*precv & 0x40) {
                    waiting = 1;
                } else if (*precv & 0x10) {
                    return CRC_ERROR;
                } else if (*precv & 0x80) {
                    return EXEC_ERROR;
                } else {
                    while(x--);
                }
                response = ataes132a->i2ctransact(deviceAddress, STATUS_REG_ADDR, NULL, precv, 0, SINGLE_RECV);
            }
            break;
        case ATAES_ENCRYPT:
            count = 9 + length;
            *packet = count;
            *(packet + 1) = ATAES_ENCRYPT;
            *(packet + 2) = 0x00; // no options
            *(packet + 3) = 0x00;
            *(packet + 4) = 0x00; // use keyid 0
            *(packet + 5) = 0x00;
            *(packet + 6) = length; // number of bytes to be encrypted
            memcpy(packet+7, data, length);
            calculateCRC(count-2, packet, crc);
            memcpy(packet+7+length, crc, 2); // copy crc to end of packet
            ataes132a->i2ctransact(deviceAddress, COMMAND_BUFFER_ADDR, packet, NULL, count, MULTIPLE_SEND);
            // wait for encryption to complete, poll status register
            while(waiting == 0) {
                response = ataes132a->i2ctransact(deviceAddress, STATUS_REG_ADDR, NULL, precv, 0, SINGLE_RECV);
                if(*precv & 0x40) {
                    waiting = 1;
                } else if (*precv & 0x10) {
                    return CRC_ERROR;
                } else if (*precv & 0x80) {
                    return EXEC_ERROR;
                } else {
                    uint32_t x = 0x0FFFFFFF;
                    while(x--);
                }
            }
            
            break;
        case ATAES_DECRYPT:
            break;
        case ATAES_NONCE:
            count = 21; // 12 bytes of nonce, 2 bytes of CRC, one byte of count
            *packet = count;
            *(packet + 1) = ATAES_NONCE;
            *(packet + 2) = 0x00; // inbound nonce mode
            *(packet + 3) = 0x00; // next 4 bytes always 0
            *(packet + 4) = 0x00;
            *(packet + 5) = 0x00;
            *(packet + 6) = 0x00; 
            
            break;
        case ATAES_GET_STATUS:
            response = ataes132a->i2ctransact(deviceAddress, STATUS_REG_ADDR, NULL, precv, 0, SINGLE_RECV);
            break;
        default:
            break;
    }
    free(crc);
    free(packet);
}

/** \This function calculates a 16-bit CRC.
 * \param[in] count number of bytes in data buffer
 * \param[in] data pointer to data
 * \param[out] crc pointer to calculated CRC (high byte at crc[0])
 */
void ataes::calculateCRC(uint8_t length, uint8_t *data, uint8_t *crc) {
    uint8_t counter;
    uint8_t crcLow = 0, crcHigh = 0, crcCarry;
    uint8_t polyLow = 0x05, polyHigh = 0x80;
    uint8_t shiftRegister;
    uint8_t dataBit, crcBit;
    for (counter = 0; counter < length; counter++) {
        for (shiftRegister = 0x80; shiftRegister > 0x00; shiftRegister >>= 1) {
            dataBit = (data[counter] & shiftRegister) ? 1 : 0;
            crcBit = crcHigh >> 7;
            // Shift CRC to the left by 1.
            crcCarry = crcLow >> 7;
            crcLow <<= 1;
            crcHigh <<= 1;
            crcHigh |= crcCarry;
            if ((dataBit ^ crcBit) != 0) {
                crcLow ^= polyLow;
                crcHigh ^= polyHigh;
            }
        }
    }
    crc[0] = crcHigh;
    crc[1] = crcLow;
}
