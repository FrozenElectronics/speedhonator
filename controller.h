/* 
 * File:   controller.h
 * Author: amr
 *
 * Created on July 4, 2020, 2:07 AM
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CONTROLLER_H
#define	CONTROLLER_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "fifo.h"

class controller {
public:
  controller();
  void updateButtonState(uint16_t);
  struct {
    uint16_t B : 1;
    uint16_t Y : 1;
    uint16_t Select : 1;
    uint16_t Start : 1;
    uint16_t Up : 1;
    uint16_t Down : 1;
    uint16_t Left : 1;
    uint16_t Right : 1;
    uint16_t A : 1;
    uint16_t X : 1;
    uint16_t L : 1;
    uint16_t R : 1;
    uint16_t null : 4;
  } buttonState;
  fifo <uint16_t,64> buttonFifo;
private:
  
};

controller::controller() {
  
  return;
}

void controller::updateButtonState(uint16_t buttons) {
    // Hopefully this actually works
    memcpy(&buttonState, &buttons, 2); // copy directly to buttonState
    return;
}

#endif	/* CONTROLLER_H */

